var request = require('request');

var baseUrl = "http://ec2-52-201-235-223.compute-1.amazonaws.com:8081";

describe("End to end test", function (done) {

    var authorities = []

    it("Should return list of local authorities", function (done) {
        request(baseUrl + "/authority/get-all", function (error, response, body) {
            console.log(body);
            expect(body).not.toBe(null);
            authorities = JSON.parse(body);

            done();
        });
    });

    it("Should be able to get report for each authority,\n " +
        "\nit should add up," +
        "\nit should be recent data", function (done) {

            authorities.forEach(function (authority) {
                request(baseUrl + "/authority/" + authority.localAuthorityId + "/report", function (error, response, body) {
                    // Check that there is data
                    expect(body).not.toBe(null);

                    // Ensure that all numbers add up!
                    var responseObject = JSON.parse(body);
                    var totalScore = 0;
                    
                    for (var key in responseObject.data) {
                        if (responseObject.data.hasOwnProperty(key) && key != "recordLastUpdated") {
                            totalScore += responseObject.data[key];
                        }
                    }
                    expect(totalScore).toBe(1);

                    // Check that data is being refreshed and it's not older than 24h                      
                    expext((responseObject.recordLastUpdated - new Date) / 36e5).toBeLessThan(24)

                });
            });            
            done();
        });
});
