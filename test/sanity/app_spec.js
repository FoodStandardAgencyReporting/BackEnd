var request = require('request');

describe("GET / - ping", function (done) {
  it("should respond with I am alive!", function (done) {
    request("http://localhost:8081/", function (error, response, body) {
      expect(body).toEqual("I am alive!");
      done();
    });
  });
});

describe("GET /authority/get-all",function(done){
  it("Should respond with 200", function(done){
    request("http://localhost:8081/authority/get-all", function (error, response, body) {
      expect(response.statusCode).toBe(200);
      done();
    });
  })
});

describe("GET /authority/:id/report",function(done){
  it("Should respond with 200", function(done){
    request("http://localhost:8081/authority/100/report", function (error, response, body) {
      expect(response.statusCode).toBe(200);
      done();
    });
  })
});