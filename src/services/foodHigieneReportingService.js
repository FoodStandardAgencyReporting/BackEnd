var http = require('request');
var settings = require('../config');

var report = [];
var authorities = [];
var ratings = {};
var headers = {
    'x-api-version': settings.X_API_VERSION
};

getAllAuthorities = function () {
    return authorities;
}

getReportForAuthorityId = function (authorityId) {
    for (var i = 0; i < report.length; i++) {
        if (report[i].localAuthorityId == authorityId) {
            return report[i];
        }
    }
};

loadRatings = function () {
    http.get({
        headers: headers,
        uri: settings.baseURL + settings.getRatingsEndPoint
    },
        function (error, response, body) {
            if (error) {
                console.log('[' + new Date().toUTCString() + ']:', error);
                return;
            }
            var responseObject = JSON.parse(body);

            responseObject.ratings.forEach(function (rating) {
                if (rating.ratingName) {
                    ratings[rating.ratingKey] = rating.ratingName;
                }
                else {
                    ratings[rating.ratingKey] = rating.ratingKey;
                }
            });
        });
}

// Raport Generation

generateReport = function () {
    http.get({
        headers: headers,
        uri: settings.baseURL + settings.listAuthoritiesEndPoint
    },
        function (error, response, body) {

            if (error) {
                console.log( '[' + new Date().toUTCString() + ']:', error);
                return;
            }

            var responseObject = JSON.parse(body);

            responseObject.authorities.forEach(
                function (authority, index) {

                    if (!settings.limitNuOfAuthorities || (index < settings.maxNoOfAuthorities)) {
                        authorities.push({
                            localAuthorityId: authority.LocalAuthorityId,
                            Name: authority.Name
                        })
                        generateReportForAuthorityId(authority.LocalAuthorityId,
                            function (authorityReport) {
                                var entry = {
                                    localAuthorityId: authority.LocalAuthorityId,
                                    Name: authority.Name,
                                    data: authorityReport,
                                    recordLastUpdated: new Date()
                                };
                                report.push(entry);
                                console.log('[' + new Date().toUTCString() + ']:', "Loaded data for:", entry.Name);
                            });
                    }
                });
        });
};

// Refresh data (every hour)
setDataRefresh = function () {
    setInterval(function () {
        console.log('[' + new Date().toUTCString() + ']:', "Starting data refresh...")
        authorities.forEach(
            function (authority) {
                generateReportForAuthorityId(authority.LocalAuthorityId,
                    function (authorityReport) {
                        var entry = {
                            localAuthorityId: authority.LocalAuthorityId,
                            Name: authority.Name,
                            data: authorityReport,
                            recordLastUpdated: new Date()
                        };
                        updateAuthorityData(entry)
                        console.log('[' + new Date().toUTCString() + ']:', "Refreshed data for:", entry.Name);
                    });
            });
    },
        1000 * 60 * settings.refreshInterval);
};

generateReportForAuthorityId = function (authorityId, callback) {
    getEstablishmentsForAuthorityId(authorityId, function (establishements) {
        var report = {};

        // Count all scores
        establishements.forEach(function (establishement) {

            ratingName = ratings[establishement.RatingKey] ? ratings[establishement.RatingKey] : establishement.RatingKey;

            if (report[ratingName] == null || report[ratingName] == undefined) {
                report[ratingName] = 0;
            }
            report[ratingName] += 1;

        });

        // turn values into percentage
        for (var key in report) {
            if (report.hasOwnProperty(key)) {
                report[key] = report[key] / establishements.length;
            }
        }
        callback(report);
    });
};

getEstablishmentsForAuthorityId = function (authorityId, callback) {
    // Initial call to check how many establishments there are
    getEstablishmentsForAuthorityIdPaged(1, 1, authorityId, function (error, response, body) {

        if (error) {
            console.log('[' + new Date().toUTCString() + ']:', error);
            return;
        }

        var responseObject = JSON.parse(body);

        // now, get all establishments
        getEstablishmentsForAuthorityIdPaged(1, responseObject.totalCount, authorityId,
            function (error, response, body) {

                if (error) {
                    console.log('[' + new Date().toUTCString() + ']:', error);
                    return;
                }

                var responseObject = JSON.parse(body);
                // pass them back to calling function
                callback(responseObject.establishments);

            });
    });
};

getEstablishmentsForAuthorityIdPaged = function (pageNumnber, pageSize, authorityId, callback) {
    http.get({
        headers: headers,
        uri: settings.baseURL + settings.getDataForAuthorityEndPoit
        + "localAuthorityId=" + authorityId + "&"
        + "pageNumber=" + pageNumnber + "&"
        + "pageSize=" + pageSize
    },
        function (error, response, body) { callback(error, response, body) });
};

updateAuthorityData = function (authorityData) {
    for (var i = 0; i < report.length; i++) {
        if (report[i].localAuthorityId == authorityData.localAuthorityId) {
            report[i] = localAuthorityId;
            return;
        }
    }
}

var exports = module.exports = {

    generateReport,
    loadRatings,
    getAllAuthorities,
    getReportForAuthorityId,
    setDataRefresh,

};