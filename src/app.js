var express = require('express');
var cors = require('cors');
var app = express();

var foodHigieneReportingService = require('./services/foodHigieneReportingService');

var port = 8081;
process.title = 'back_end';

app.use(cors());

app.get('/', function (req, res) {
    res.send('I am alive!');
});

app.get('/authority/get-all', function (req, res) {
    res.send(JSON.stringify(foodHigieneReportingService.getAllAuthorities()))
});

app.get('/authority/:id/report', function (req, res) {
    var authorityId = req.params.id;
    res.send(foodHigieneReportingService.getReportForAuthorityId(authorityId));
});

var server = app.listen(port, function () {
    console.log('[' + new Date().toUTCString() + ']:',"Started gathering data ...");

    foodHigieneReportingService.loadRatings();
    foodHigieneReportingService.generateReport();
    foodHigieneReportingService.setDataRefresh();
    
    console.log('[' + new Date().toUTCString() + ']:','Backend server listening on port: ', port);
});

exports.closeServer = function () {
    server.close();
};