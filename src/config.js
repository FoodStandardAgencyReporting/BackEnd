var exports = module.exports = {
    serverPort: 8081,
    baseURL: "http://api.ratings.food.gov.uk",
    X_API_VERSION: 2,
    listAuthoritiesEndPoint: "/Authorities/basic",
    getDataForAuthorityEndPoit: "/Establishments?",
    getRatingsEndPoint : "/Ratings",
    refreshInterval : 60, // Data refresh interval in minutes
    limitNuOfAuthorities : true,
    maxNoOfAuthorities : 50 // Maximum number of reports to pull
}