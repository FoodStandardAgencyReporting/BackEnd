**Requirements:**
1. npm
2. port 8081 open

**To Run:**
1. npm install
2. npm start

To run tests:
npm test - sanity tests
npm run endtoend - end to end tests

**Description:**

This NodeJs app aggregates food higiene data using API from http://api.ratings.food.gov.uk. 

**How this thing works:**

Once started, it will call food ratings for:
1. List of all grades
2. List of all local authorities
3. List of all establishements for each local authority
 
It will then go through the data and calculate statistics for each authority

After that, the data will be periodically refreshed.

**Settings:**

    serverPort: 8081,
    baseURL: "http://api.ratings.food.gov.uk",
    X_API_VERSION: 2,
    listAuthoritiesEndPoint: "/Authorities/basic",
    getDataForAuthorityEndPoit: "/Establishments?",
    getRatingsEndPoint : "/Ratings",
    refreshInterval : 60, // Data refresh interval in minutes
    maxNoOfAuthorities : 20 // Maximum number of reports to pull

**Known issues:**

Food rating service data quality is poor. One of the issues is that some establishments have scores with keys that do not exist in the overall list of scores. 
Due to this issue, some scores will not have a corresponding "frindly" name and the key will be outputed instead. 
This issue would be fixed (without changing any code in this application) if the data would be corrected. 

Slow startup - it takes time to calculate the report. Report is available before it's completed, so at startup time, service will provide inpomplete data. 
